# Changelog

## 0.8.1

- upgraded crypto package to 0.9.2+1                             

## 0.8.1

* increase upper bound on uri package 

## 0.8.0

* ClientFactory now returns a Future

## 0.7.0

* Renamed OAuth2Provider to OAuth2AuthorizationServer and similar for oauth1
* Added some common authorization servers like github, bitbucket, google
  and hipchat

## 0.6.0

* shelf 0.6.0
* OAuth2ProviderHandlers take factories for ClientId and OAuth2Provider
* upgrade uri and test packages

## 0.5.0

* Now uses http_exception package rather than shelf_exception_response

## 0.4.0

* make accessType a parameter. Note: default is now 'online' whereas previously
it was 'offline'

## 0.3.1

* added OAuthStorage for conveniently grouping all oauth related storage

## 0.3.0

* storing tokens in oauth 2 now optional
* fixed handling of content-type
* store context for oauth2

## 0.2.0

* Slightly more robust handling of content-type

## 0.1.0

* Added oauth2

## 0.0.2

* some validation

## 0.0.1

* Initial version, created by Stagehand.
