// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.oauth.storage.inmem;

import 'oauth1.dart';
import 'oauth2.dart';
import 'oauth_storage.dart';
import 'package:option/option.dart';
import 'dart:async';

/// An in memory implementation of [OAuth2CSRFStateStore] suitable for
/// testing only
class InMemoryOAuth2CSRFStateStore implements OAuth2CSRFStateStore {
  final Map<String, OAuth2State> tokenMap = {};

  @override
  Future storeState(OAuth2State state) {
    tokenMap[state.state] = state;
    return new Future.value();
  }

  @override
  Future<Option<OAuth2State>> consumeState(String state) {
    return new Future.value(new Option(tokenMap.remove(state)));
  }
}

class InMemoryOAuth2TokenStore implements OAuth2TokenStore {
  final Map<String, OAuth2TokenPair> tokenMap = {};

  @override
  Future storeToken(String sessionIdentifier, OAuth2TokenPair tokenPair) {
    tokenMap[sessionIdentifier] = tokenPair;
    return new Future.value();
  }

  @override
  Future<Option<OAuth2TokenPair>> fetchToken(String sessionIdentifier) {
    return new Future.value(new Option(tokenMap[sessionIdentifier]));
  }
}

/// An in memory implementation of [OAuth1RequestTokenSecretStore] suitable for
/// testing only
class InMemoryOAuth1RequestTokenSecretStore
    implements OAuth1RequestTokenSecretStore {
  final Map<String, String> tokenMap = {};

  @override
  Future storeSecret(String authToken, String secret) {
    tokenMap[authToken] = secret;
    return new Future.value();
  }

  @override
  Future<Option<String>> consumeSecret(String authToken) {
    return new Future.value(new Option(tokenMap.remove(authToken)));
  }
}

/// stores OAuth data in memory. Typically not suitable for production
class InMemoryOAuthStorage implements OAuthStorage {
  final OAuth1RequestTokenSecretStore oauth1TokenSecretStore =
      new InMemoryOAuth1RequestTokenSecretStore();

  final OAuth2TokenStore oauth2TokenStore = new InMemoryOAuth2TokenStore();

  final OAuth2CSRFStateStore oauth2CSRFStateStore =
      new InMemoryOAuth2CSRFStateStore();
}
