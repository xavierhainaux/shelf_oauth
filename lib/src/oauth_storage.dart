// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.oauth.storage;

import 'oauth1.dart';
import 'oauth2.dart';

/// Provides storage for the various OAuth items
abstract class OAuthStorage {
  OAuth1RequestTokenSecretStore get oauth1TokenSecretStore;
  OAuth2CSRFStateStore get oauth2CSRFStateStore;
  OAuth2TokenStore get oauth2TokenStore;
}
