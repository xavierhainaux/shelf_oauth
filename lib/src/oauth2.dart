// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.oauth2;

import 'package:shelf/shelf.dart';
import 'dart:async';
import 'package:option/option.dart';
import 'oauth2_impl.dart';
import 'package:uri/uri.dart';
import 'package:shelf_oauth/src/preconditions.dart';

/// A factory that creates a [ClientId] from a [Request]
typedef Future<ClientId> ClientIdFactory(Request r);

/// A factory that creates a [OAuth2AuthorizationServer] from a [Request]
typedef OAuth2AuthorizationServer OAuth2AuthorizationServerFactory(Request r);

/// Used to extract some kind of identifier of a user session from a [Request]
typedef Future<String> SessionIdentifierExtractor(Request request);

/// Represents the client application's credentials.
class ClientId {
  /// The identifier used to identify this application to the server.
  final String identifier;

  /// The client secret used to identify this application to the server.
  final String secret;

  ClientId(this.identifier, this.secret) {
    if (identifier == null) {
      throw new ArgumentError('Argument identifier may not be null.');
    }
  }
}

/// A provider of OAuth2 services. Captures the relevant details for an
/// 'Authorization Server' that we will interact with as part of the
/// [Authorization Code Flow](http://tools.ietf.org/html/rfc6749#section-4.1).
///
/// We start by redirecting the user to the [authorizationUrlTemplate] for them
/// to grant us access. We are then redirected back with an authorization code
/// which we then pass to the provider's [accessTokenUrl]
class OAuth2AuthorizationServer {
  /// The url of the authorization endpoint
  final UriTemplate authorizationUrlTemplate;

  /// The url of the access token endpoint
  final Uri accessTokenUrl;

  final String providerName;

  const OAuth2AuthorizationServer(
      this.providerName, this.authorizationUrlTemplate, this.accessTokenUrl);

  OAuth2AuthorizationServer.std(
      String providerName, Uri baseAuthorizationUrl, Uri accessTokenUrl)
      : this(providerName, oauth2AuthorizationTemplate(baseAuthorizationUrl),
            accessTokenUrl);

  Uri authorizationUrl(String responseType, String clientId, String state,
          List<String> scopes, Uri redirectUri,
          {String accessType: 'online'}) =>
      Uri.parse(authorizationUrlTemplate.expand({
        'response_type': responseType,
        'client_id': clientId,
        'state': state,
        'scope': scopes.join(' '),
        'access_type': accessType,
        'redirect_uri': redirectUri.toString()
      }));

  String accessTokenBody(String grantType, String authorizationCode,
      String clientId, String clientSecret, Uri redirectUri) {
    final Map bodyMap = {
      'grant_type': grantType,
      'code': authorizationCode,
      'client_id': clientId,
      'client_secret': clientSecret, // TODO: is this standard?
      'redirect_uri': redirectUri.toString()
    };

    final body = bodyMap.keys
        .map((k) => '$k=${Uri.encodeComponent(bodyMap[k])}')
        .join('&');

    return body;
  }
}

/// shelf [Handler]'s for the oauth2 dance
abstract class OAuth2ProviderHandlers {
  factory OAuth2ProviderHandlers(
      ClientIdFactory clientIdFactory,
      OAuth2AuthorizationServerFactory oauthProviderFactory,
      Uri redirectUrl,
      OAuth2CSRFStateStore stateStore,
      OAuth2TokenStore tokenStore,
      UriTemplate completionRedirectUrl,
      SessionIdentifierExtractor sessionIdExtractor,
      List<String> scopes,
      {bool storeTokens: true,
      String accessType: 'online'}) {
    return new OAuth2ProviderHandlersImpl(
        clientIdFactory,
        oauthProviderFactory,
        redirectUrl,
        stateStore,
        tokenStore,
        completionRedirectUrl,
        sessionIdExtractor,
        scopes,
        storeTokens,
        accessType);
  }

  Handler authorizationRequestHandler();

  Handler accessTokenRequestHandler();
}

class OAuth2ResponseType {
  static const String code = 'code';
}

class OAuth2GrantType {
  static const String authorizationCode = 'authorization_code';
}

class OAuth2State {
  final String state;
  final String sessionIdentifier;
  final Option<String> context;

  OAuth2State(this.state, this.sessionIdentifier, this.context) {
    ensure(state, isNotNull);
    ensure(sessionIdentifier, isNotNull);
    ensure(context, isNotNull);
  }

  OAuth2State.fromJson(Map json)
      : this(json['state'], json['sessionIdentifier'],
            new Option(json['context']));

  Map toJson() {
    final m = {'state': state, 'sessionIdentifier': sessionIdentifier};
    _add(m, 'context', context);
    return m;
  }
}

class OAuth2TokenPair {
  final String accessToken;
  final Option<String> refreshToken;
  final Option<DateTime> expires;

  /// if None then assume all requested scopes were granted
  final Option<List<String>> grantedScopes;

  OAuth2TokenPair(
      this.accessToken, this.refreshToken, this.expires, this.grantedScopes) {
    ensure(accessToken, isNotNull);
    ensure(refreshToken, isNotNull);
    ensure(expires, isNotNull);
    ensure(grantedScopes, isNotNull);
  }

  OAuth2TokenPair.fromJson(Map json)
      : this(
            json['accessToken'],
            new Option(json['refreshToken']),
            new Option(json['expires']).map((exp) => DateTime.parse(exp)),
            new Option(_toList(json['grantedScopes'])));

  Map toJson() {
    final m = {'accessToken': accessToken};
    _add(m, 'refreshToken', refreshToken);
    _add(m, 'expires', expires.map((exp) => exp.toIso8601String()));
    _add(m, 'grantedScopes', grantedScopes);
    return m;
  }
}

// sadly bitbucket returns a string if it is a single value
List<String> _toList(value) {
  if (value == null) {
    return const [];
  } else if (value is Iterable) {
    return value.toList();
  } else {
    return [value.toString()];
  }
}

void _add(Map map, String key, Option value) {
  if (value is Some) {
    map[key] = value.get();
  }
}

/// A store for OAuth2 request 'state' parameters which are used to prevent
/// CSRF attacks on the redirection Uri. State values are only stored for a
/// short time during the oauth2 dance
abstract class OAuth2CSRFStateStore {
  // TODO: need to include oauth provider as part of key as the authToken's
  // will not be guaranteed to be unique across providers. Although probably
  // low odds of getting duped tokens given they are short lived.

  /// stores the secret for the token
  Future storeState(OAuth2State state);

  /// retrieves and removes the secret for the token
  Future<Option<OAuth2State>> consumeState(String state);
}

abstract class OAuth2TokenStore {
  Future storeToken(String sessionIdentifier, OAuth2TokenPair tokenPair);

  Future<Option<OAuth2TokenPair>> fetchToken(String sessionIdentifier);
}

UriTemplate oauth2AuthorizationTemplate(Uri baseUri) {
  final template = baseUri.toString() +
      '{?response_type,client_id,state,scope,access_type,redirect_uri}';
  return new UriTemplate(template);
}
