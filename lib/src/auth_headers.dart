// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

// TODO: find another home for this

library shelf_oauth.oauth.headers;

import 'dart:io';
import 'dart:convert';

abstract class AuthCredentials {
  String encode();
}

class AuthorizationHeaders {
  final List<AuthorizationHeader> authHeaders;

  AuthorizationHeaders(this.authHeaders);

  Map encode() => {HttpHeaders.AUTHORIZATION: _encodedHeaders};

  String get _encodedHeaders => authHeaders.map((h) => h.encode()).join(',');
}

class AuthorizationHeader<T extends AuthCredentials> {
  final String authScheme;
  final T credentials;

  AuthorizationHeader(this.authScheme, this.credentials);

  String encode() => '${authScheme} ${credentials.encode()}';
}

const BASIC_AUTH_SCHEME = 'Basic';

class BasicAuthHeader extends AuthorizationHeader {
  BasicAuthHeader(String username, String password)
      : super(BASIC_AUTH_SCHEME, new BasicAuthCredentials(username, password));
}

class BasicAuthCredentials implements AuthCredentials {
  final String username;
  final String password;

  BasicAuthCredentials(this.username, this.password);

  factory BasicAuthCredentials.decode(String encoded) {
    final usernamePasswordStr = new String.fromCharCodes(
        const Base64Codec.urlSafe().decode(encoded));

    final usernamePassword = usernamePasswordStr.split(':');

    if (usernamePassword.length != 2) {
      throw new ArgumentError('invalid basic auth credentials');
    }

    return new BasicAuthCredentials(usernamePassword[0], usernamePassword[1]);
  }

  String encode() =>
      const Base64Codec.urlSafe().encode('$username:$password'.codeUnits);
}
