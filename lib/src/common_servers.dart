// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.authorization_servers;

import 'package:shelf_oauth/src/oauth1.dart';
import 'package:shelf_oauth/src/oauth2.dart';

/// a collection of known common oauth authorization servers
CommonAuthorizationServers get commonAuthorizationServers =>
    new CommonAuthorizationServers();

abstract class CommonAuthorizationServers {
  OAuth1AuthorizationServer get bitBucketOAuth1;

  OAuth2AuthorizationServer get bitbucketOAuth2;

  OAuth2AuthorizationServer get gitHubOAuth2;

  OAuth2AuthorizationServer get hipchatOAuth2;

  OAuth2AuthorizationServer get googleOAuth2;

  factory CommonAuthorizationServers() = _CommonAuthorizationServersImpl;
}

class _CommonAuthorizationServersImpl implements CommonAuthorizationServers {
  static const _bitBucketOAuth1UrlBase = 'https://bitbucket.org/api/1.0/oauth';

  OAuth1AuthorizationServer get bitBucketOAuth1 =>
      new OAuth1AuthorizationServer(
          Uri.parse('$_bitBucketOAuth1UrlBase/request_token'),
          Uri.parse('$_bitBucketOAuth1UrlBase/access_token'),
          Uri.parse('$_bitBucketOAuth1UrlBase/authenticate'));

  static const _githubOAuthUrlBase = 'https://github.com/login/oauth';

  OAuth2AuthorizationServer get gitHubOAuth2 =>
      new OAuth2AuthorizationServer.std(
          'github',
          Uri.parse('$_githubOAuthUrlBase/authorize'),
          Uri.parse('$_githubOAuthUrlBase/access_token'));

  static const _bitBucketOAuth2UrlBase = 'https://bitbucket.org/site/oauth2';

  OAuth2AuthorizationServer get bitbucketOAuth2 =>
      new OAuth2AuthorizationServer.std(
          'bitbucket',
          Uri.parse('$_bitBucketOAuth2UrlBase/authorize'),
          Uri.parse('$_bitBucketOAuth2UrlBase/access_token'));

  static const _hipchatOAuth2UrlBase = 'https://www.hipchat.com';

  OAuth2AuthorizationServer get hipchatOAuth2 =>
      new OAuth2AuthorizationServer.std(
          'hipchat',
          Uri.parse('$_hipchatOAuth2UrlBase/users/authorize'),
          Uri.parse('$_hipchatOAuth2UrlBase/v2/oauth/token'));

  OAuth2AuthorizationServer get googleOAuth2 =>
      new OAuth2AuthorizationServer.std(
          'google',
          Uri.parse('https://accounts.google.com/o/oauth2/auth'),
          Uri.parse('https://www.googleapis.com/oauth2/v3/token'));
}
