// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.oauth1.impl;

import 'package:shelf/shelf.dart';
import 'package:oauth/oauth.dart' as oauth;
import 'package:uri/uri.dart';
import 'package:http/http.dart' as http;
import 'dart:math';
import 'dart:async';
import 'dart:convert';
import 'package:http_exception/http_exception.dart';
import 'oauth1.dart';
import 'preconditions.dart';
import 'package:path/path.dart' as pathos;
import 'package:logging/logging.dart';

final _p = pathos.url;

const String _OAUTH_TOKEN = 'oauth_token';
const String _OAUTH_TOKEN_SECRET = 'oauth_token_secret';

Logger _log = new Logger('shelf_oauth.oauth1');

class OAuth1ProviderHandlersImpl implements OAuth1ProviderHandlers {
  final OAuth1Token consumerToken;
  final OAuth1AuthorizationServerFactory oAuth1AuthorizationServerFactory;
  final String callbackUrlStr;
  final OAuth1RequestTokenSecretStore tokenStore;
  final UriTemplate completionRedirectUrl;

  // TODO: makes sense to let callbackUrl be absolute or relative. If relative
  // then relative to basepath
  OAuth1ProviderHandlersImpl(
      this.consumerToken,
      this.oAuth1AuthorizationServerFactory,
      this.callbackUrlStr,
      this.tokenStore,
      this.completionRedirectUrl) {
    ensure(consumerToken, isNotNull);
    ensure(oAuth1AuthorizationServerFactory, isNotNull);
    ensure(callbackUrlStr, isNotNull);
    ensure(tokenStore, isNotNull);
    ensure(completionRedirectUrl, isNotNull);
  }

  Handler tokenRequestHandler() => _redirectForUserAuthentication;

  Handler accessTokenRequestHandler() => _fetchAccessToken;

  Future<Response> _redirectForUserAuthentication(Request req) async {
    final oAuth1AuthorizationServer = oAuth1AuthorizationServerFactory(req);

    final requestTokenUrl =
        _createRequestTokenUrl(req, oAuth1AuthorizationServer);

    final oauthProviderClient = new http.IOClient();

    final response = await oauthProviderClient.post(requestTokenUrl);

    return await _processRequestTokenResponse(
        response, oAuth1AuthorizationServer);
  }

  Future<Response> _processRequestTokenResponse(http.Response response,
      OAuth1AuthorizationServer oAuth1AuthorizationServer) {
    final statusCode = response.statusCode;
    _log.finer('_processRequestTokenResponse: statusCode: $statusCode');
    if (statusCode >= 200 && statusCode < 300) {
      final body = response.body;
      final m = Uri.splitQueryString(body);
      final authToken = m[_OAUTH_TOKEN];
      final authTokenSecret = m[_OAUTH_TOKEN_SECRET];

      _log.finest('_processRequestTokenResponse: authToken: $authToken; '
          'authTokenSecret: $authTokenSecret');

      return tokenStore.storeSecret(authToken, authTokenSecret).then((_) {
        final authUri =
            (new UriBuilder.fromUri(oAuth1AuthorizationServer.authenticationUrl)
                  ..queryParameters = {_OAUTH_TOKEN: authToken})
                .build();

        // redirect for user authentication
        return new Response.found(authUri);
      });
    } else {
      throw new BadRequestException(
          null, 'failed to authenticate: response code was $statusCode');
    }
  }

  Future<Response> _fetchAccessToken(Request req) {
    _log.finest('_fetchAccessToken: ${req.requestedUri}');

    final context = req.requestedUri.queryParameters['context'];

    final queryParams = req.url.queryParameters;
    final authVerifier = queryParams['oauth_verifier'];
    final authToken = queryParams[_OAUTH_TOKEN];

    return tokenStore.consumeSecret(authToken).then((secretOpt) {
      return secretOpt
          .map((secret) => _requestAccessToken(authToken, secret, authVerifier,
              context, oAuth1AuthorizationServerFactory(req)))
          .getOrElse(() {
        // token either expired or dodgy
        throw new UnauthorizedException({}, 'no matching token');
      });
    });
  }

  Future<Response> _requestAccessToken(
      String authToken,
      String secret,
      String authVerifier,
      String context,
      OAuth1AuthorizationServer oAuth1AuthorizationServer) {
    final authTokenUrl = _createAuthTokenUrl(
        authToken, secret, authVerifier, oAuth1AuthorizationServer);

    final oauthProviderClient = new http.IOClient();

    return oauthProviderClient.post(authTokenUrl).then(
        (http.Response response) =>
            _processAuthTokenResponse(response, context));
  }

  Response _processAuthTokenResponse(http.Response response, String context) {
    final statusCode = response.statusCode;
    _log.finer('_processAuthTokenResponse: statusCode: $statusCode');
    if (statusCode >= 200 && statusCode < 300) {
      final body = response.body;
      final m = Uri.splitQueryString(body);
      final authToken = m[_OAUTH_TOKEN];
      final authTokenSecret = m[_OAUTH_TOKEN_SECRET];

      _log.finest('_processAuthTokenResponse: authToken: $authToken; '
          'authTokenSecret: $authTokenSecret');

      final redirectUrl = completionRedirectUrl.expand({
        'context': context,
        'token': authToken,
        'type': 'oauth1',
        'secret': authTokenSecret
      });

      _log.finest('redirecting to $redirectUrl');

      return new Response.found(redirectUrl);
    } else {
      throw new HttpException(statusCode,
          'Failed to upgrade to authToken: response code was $statusCode');
    }
  }

  Uri _createRequestTokenUrl(
      Request req, OAuth1AuthorizationServer oAuth1AuthorizationServer) {
    // I know a bit weird but route paths tend to start with /
    final isRelative =
        callbackUrlStr.startsWith('/') || _p.isRelative(callbackUrlStr);

    final context = req.requestedUri.queryParameters['context'];

    final path = _p.join(_p.dirname(req.handlerPath), callbackUrlStr);

    final builder = isRelative
        ? (new UriBuilder.fromUri(req.requestedUri)..path = path)
        : new UriBuilder.fromUri(Uri.parse(callbackUrlStr));

    builder.queryParameters = new Map.from(builder.queryParameters)
      ..['context'] = context;

    builder.fragment = null;

    final callbackUrl = builder.toString();

    _log.finest('_createRequestTokenUrl: callback $callbackUrl');

    var requestTokenUri =
        (new UriBuilder.fromUri(oAuth1AuthorizationServer.requestTokenUrl)
              ..queryParameters = {'oauth_callback': callbackUrl}
              ..fragment = null)
            .build();

    final request = new http.Request("POST", requestTokenUri);
    final params = oauth.generateParameters(
        request,
        _tokens(consumerToken, null),
        _nonce(),
        new DateTime.now().millisecondsSinceEpoch ~/ 1000);

    final fullUrl = (new UriBuilder.fromUri(requestTokenUri)
          ..queryParameters.addAll(params)
          ..fragment = null)
        .build();

    _log.finest('_createRequestTokenUrl: requestTokenUri is $fullUrl');

    return fullUrl;
  }

  Uri _createAuthTokenUrl(String authToken, String secret, String authVerifier,
      OAuth1AuthorizationServer oAuth1AuthorizationServer) {
    OAuth1Token userToken = new OAuth1Token(authToken, secret);

    _log.finest('_createAuthTokenUrl: authVerifier: $authVerifier; '
        'authToken: $authToken');

    var accessTokenUri =
        (new UriBuilder.fromUri(oAuth1AuthorizationServer.accessTokenUrl)
              ..queryParameters = {'oauth_verifier': authVerifier})
            .build();

    final request = new http.Request("POST", accessTokenUri);
    final params = oauth.generateParameters(
        request,
        _tokens(consumerToken, userToken),
        _nonce(15),
        new DateTime.now().millisecondsSinceEpoch ~/ 1000);

    final fullUrl = (new UriBuilder.fromUri(accessTokenUri)
          ..queryParameters.addAll(params))
        .build();

    _log.finest('_createAuthTokenUrl: accessTokenUri is $fullUrl');

    return fullUrl;
  }
}

var _random = new Random();

String _nonce([int size = 8]) {
  final n = new List<int>.generate(size, (_) => _random.nextInt(255),
      growable: false);
  String nonceStr = const Base64Codec.urlSafe().encode(n);
  return nonceStr;
}

oauth.Tokens _tokens(OAuth1Token consumerToken, OAuth1Token userToken) {
  return userToken != null
      ? new oauth.Tokens(
          consumerId: consumerToken.token,
          consumerKey: consumerToken.secret,
          userId: userToken.token,
          userKey: userToken.secret)
      : new oauth.Tokens(
          consumerId: consumerToken.token, consumerKey: consumerToken.secret);
}
