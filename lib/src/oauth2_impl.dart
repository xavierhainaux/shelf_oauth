// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.oauth2.impl;

import 'package:shelf/shelf.dart';
import 'package:uri/uri.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:http_exception/http_exception.dart';
import 'oauth2.dart';
import 'preconditions.dart';
import 'package:path/path.dart' as pathos;
import 'package:logging/logging.dart';
import 'package:option/option.dart';
import 'package:uuid/uuid.dart';
import 'auth_headers.dart';
import 'dart:convert';
import 'dart:io';

final _p = pathos.url;

const String _ACCESS_TOKEN = 'access_token';
//const String _OAUTH_TOKEN_SECRET = 'oauth_token_secret';

Logger _log = new Logger('shelf_oauth.oauth2');

class OAuth2ProviderHandlersImpl implements OAuth2ProviderHandlers {
  final ClientIdFactory clientIdFactory;
  final OAuth2AuthorizationServerFactory authorizationServerFactory;
  final Uri redirectUrl;
  final OAuth2CSRFStateStore stateStore;
  final bool storeTokens;
  final OAuth2TokenStore tokenStore;
  final UriTemplate completionRedirectUrl;
  final SessionIdentifierExtractor sessionIdExtractor;
  final List<String> scopes;
  final String accessType;

  // TODO: makes sense to let callbackUrl be absolute or relative. If relative
  // then relative to basePath
  OAuth2ProviderHandlersImpl(
      this.clientIdFactory,
      this.authorizationServerFactory,
      this.redirectUrl,
      this.stateStore,
      this.tokenStore,
      this.completionRedirectUrl,
      this.sessionIdExtractor,
      this.scopes,
      this.storeTokens,
      this.accessType) {
    ensure(clientIdFactory, isNotNull);
    ensure(authorizationServerFactory, isNotNull);
    ensure(redirectUrl, isNotNull);
    ensure(stateStore, isNotNull);
    ensure(tokenStore, isNotNull);
    ensure(completionRedirectUrl, isNotNull);
    ensure(sessionIdExtractor, isNotNull);
    ensure(scopes, isNotNull);
    ensure(storeTokens, isNotNull);
    ensure(accessType, isNotNull);
  }

  @override
  Handler authorizationRequestHandler() => _redirectForUserAuthentication;

  @override
  Handler accessTokenRequestHandler() => _exchangeCodeForTokenAndRedirect;

  Future<Response> _redirectForUserAuthentication(Request req) {
    _log.finest('_redirectForUserAuthentication');

    return sessionIdExtractor(req).then((sessionId) async {
      final state = new Uuid().v4();
      final context = new Option(req.requestedUri.queryParameters['context']);

      await stateStore.storeState(new OAuth2State(state, sessionId, context));
      final authUri = authorizationServerFactory(req).authorizationUrl(
          OAuth2ResponseType.code,
          (await clientIdFactory(req)).identifier,
          state,
          scopes,
          _createRedirectUrl(req),
          accessType: accessType);

      // redirect for user authentication
      return new Response.found(authUri);
    });
  }

  Future<Response> _exchangeCodeForTokenAndRedirect(Request req) async {
    _log.finest('_exchangeCodeForTokenAndRedirect');

    final params = req.requestedUri.queryParameters;
    final state = params['state'];
    if (state == null) {
      _badRequest('no state param in request');
    }

    final savedStateOpt = await stateStore.consumeState(state);

    return savedStateOpt.map((OAuth2State savedState) async {
//        _checkSessionId(savedState, req);
      final OAuth2TokenPair tokenPair =
          await _exchangeCodeForToken(req, params);

      _log.finest('tokenPair: ${tokenPair.toJson()}');

      await _storeTokens(savedState, tokenPair);

      final context = savedState.context.getOrElse(() => '');
      final scopes =
          tokenPair.grantedScopes.map((s) => s.join(',')).getOrElse(() => null);
      // do redirect
      // TODO: not sure it is useful in general to send access token
      // to client. Note if the template doesn't include it we won't pass
      // it so maybe ok
      final redirectUrl = completionRedirectUrl.expand({
        'context': context,
        'token': tokenPair.accessToken,
        'type': 'oauth2',
        'sessionId': savedState.sessionIdentifier,
        'scopes': scopes
      });

      _log.finest('redirecting to $redirectUrl');

      return new Response.found(redirectUrl);
    }).getOrElse(() => _badRequest('invalid state in request $state'));
  }

  Future _storeTokens(OAuth2State savedState, OAuth2TokenPair tokenPair) {
    return storeTokens
        ? tokenStore.storeToken(savedState.sessionIdentifier, tokenPair)
        : new Future.value();
  }

  Future<OAuth2TokenPair> _exchangeCodeForToken(Request req, Map params) async {
    final authorizationCode = params['code'];
    if (authorizationCode == null) {
      _badRequest('missing code parameter in request');
    }

    final clientId = await clientIdFactory(req);
    final oauthProvider = authorizationServerFactory(req);

    final authHeaders = new AuthorizationHeaders(
        [new BasicAuthHeader(clientId.identifier, clientId.secret)]).encode();

    final headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': ContentType.JSON.mimeType
    }..addAll(authHeaders);

    final oauthProviderClient = new http.IOClient();
    final accessTokenBody = oauthProvider.accessTokenBody(
        OAuth2GrantType.authorizationCode,
        authorizationCode,
        clientId.identifier,
        clientId.secret,
        _createRedirectUrl(req));

    _log.finest(accessTokenBody);

    final response = await oauthProviderClient.post(
        oauthProvider.accessTokenUrl,
        headers: headers,
        body: accessTokenBody);

    return _processAuthTokenResponse(response);
  }

  OAuth2TokenPair _processAuthTokenResponse(http.Response response) {
    final statusCode = response.statusCode;
    _log.finer('_processAuthTokenResponse: statusCode: $statusCode');
    if (statusCode >= 200 && statusCode < 300) {
      final contentType = ContentType.parse(response.headers['content-type']);
      if (contentType.mimeType != ContentType.JSON.mimeType) {
        _notImplemented('only json responses supported');
      }
      final body = response.body;
      try {
        final m = JSON.decode(body);
        final accessToken = m[_ACCESS_TOKEN];
        final refreshToken = new Option(m['refresh_token']);
        final expiresIn = new Option(m['expires_in']);
        final expires = expiresIn.map(
            (expIn) => new DateTime.now().add(new Duration(seconds: expIn)));
        final grantedScopes = new Option(_toList(m['scopes']));

        return new OAuth2TokenPair(
            accessToken, refreshToken, expires, grantedScopes);
      } on FormatException catch (_) {
        _notImplemented(
            'Unable to parse body as json. Only json responses supported');
        return null;
      }
    } else {
      _log.finest(response.body);
      _badRequest('failed to exchange code for token'); // TODO: more like a 403
      return null;
    }
  }

//  void _checkSessionId(OAuth2State savedState, Request req) {
//    final sessionId = sessionIdExtractor(req);
//    if (sessionId != savedState.sessionIdentifier) {
//      _badRequest('session identifier does not match');
//    }
//  }

  Uri _createRedirectUrl(Request req) {
    final callbackUrlStr = redirectUrl.toString();
    // I know a bit weird but route paths tend to start with /
    final isRelative =
        callbackUrlStr.startsWith('/') || _p.isRelative(callbackUrlStr);

//    final context = req.requestedUri.queryParameters['context'];

    final path = _p.join(_p.dirname(req.handlerPath), callbackUrlStr);

    final builder = isRelative
        ? (new UriBuilder.fromUri(req.requestedUri)..path = path)
        : new UriBuilder.fromUri(Uri.parse(callbackUrlStr));

    builder.queryParameters = null;
//        new Map.from(builder.queryParameters);
//          ..['context']=context;

    builder.fragment = null;

    final callbackUrl = builder.build();

    _log.finest('_createRequestTokenUrl: callback $callbackUrl');
    return callbackUrl;
  }
}

void _badRequest(String msg) {
  throw new BadRequestException({'error': msg}, msg);
}

void _notImplemented(String msg) {
  throw new NotImplementedException({'error': msg}, msg);
}

// sadly bitbucket returns a string if it is a single value
List<String> _toList(value) {
  if (value == null) {
    return const [];
  } else if (value is Iterable) {
    return value.toList();
  } else {
    return [value.toString()];
  }
}
