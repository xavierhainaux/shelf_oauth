// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.oauth1;

import 'package:shelf/shelf.dart';
import 'dart:async';
import 'package:option/option.dart';
import 'oauth1_impl.dart';
import 'package:uri/uri.dart';
import 'token.dart';
export 'token.dart';

/// A factory that creates a [OAuth1AuthorizationServer] from a [Request]
typedef OAuth1AuthorizationServer OAuth1AuthorizationServerFactory(Request r);

/// A provider of OAuth 1 services
class OAuth1AuthorizationServer {
  final Uri requestTokenUrl;
  final Uri accessTokenUrl;
  final Uri authenticationUrl;

  const OAuth1AuthorizationServer(
      this.requestTokenUrl, this.accessTokenUrl, this.authenticationUrl);
}

/// A store for OAuth1 request token secrets. Tokens are only stored for a
/// short time during the oauth1 dance
abstract class OAuth1RequestTokenSecretStore {
  // TODO: need to include oauth provider as part of key as the authToken's
  // will not be guaranteed to be unique across providers. Although probably
  // low odds of getting duped tokens given the are short lived.

  /// stores the secret for the token
  Future storeSecret(String authToken, String secret);

  /// retrieves and removes the secret for the token
  Future<Option<String>> consumeSecret(String authToken);
}

/// shelf [Handler]'s for the oauth1 dance
abstract class OAuth1ProviderHandlers {
  factory OAuth1ProviderHandlers(
      OAuth1Token consumerToken,
      OAuth1AuthorizationServerFactory authorizationServerFactory,
      String callbackUrl,
      OAuth1RequestTokenSecretStore tokenStore,
      UriTemplate completionRedirectUrl) {
    return new OAuth1ProviderHandlersImpl(
        consumerToken,
        authorizationServerFactory,
        callbackUrl,
        tokenStore,
        completionRedirectUrl);
  }

  Handler tokenRequestHandler();

  Handler accessTokenRequestHandler();
}
