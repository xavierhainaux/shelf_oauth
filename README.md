# shelf_oauth

[![Build Status](https://drone.io/bitbucket.org/andersmholmgren/shelf_oauth/status.png)](https://drone.io/bitbucket.org/andersmholmgren/shelf_oauth/latest)
[![Pub Version](http://img.shields.io/pub/v/shelf_oauth.svg)](https://pub.dartlang.org/packages/shelf_oauth)

Provides Shelf Handlers to do the OAuth dance. Supports OAuth1 and OAuth2

## Usage

TODO...

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/andersmholmgren/shelf_oauth/issues
